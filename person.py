from dataclasses import dataclass
from datetime import date
from requests import get as requests_get


@dataclass
class Person:
    name: str
    birth_date: date
    address: str

    def __str__(self):
        return self.name

    @property
    def short_name(self):
        return self.name.split(' ')[0]

    def age(self):
        return round((date.today() - self.birth_date).days / 365.0, 1)

    def _get_lat_lng(self):
        """
        Convert the Persons address into a latitude/longitude

        Uses the https://geocoding.geo.census.gov/ API
        """
        response = requests_get('https://geocoding.geo.census.gov/geocoder/locations/onelineaddress', {
            'benchmark': 'Public_AR_Current',
            'vintage': 'Current',
            'format': 'json',
            'address': self.address,
        })

        if response.status_code == 200:
            response_json = response.json()

            if response_json['result']['addressMatches']:
                lat = response_json['result']['addressMatches'][0]['coordinates']['y']
                lng = response_json['result']['addressMatches'][0]['coordinates']['x']

                return (lat, lng)

        return (None, None)

    def _get_weather_points(self, lat, lng):
        """
        Lookup the weather points for the lat/lng

        Uses the https://www.weather.gov/documentation/services-web-api API
        """
        response = requests_get(f'https://api.weather.gov/points/{lat},{lng}')

        if response.status_code == 200:
            response_json = response.json()

            grid_id = response_json['properties']['gridId']
            grid_x = response_json['properties']['gridX']
            grid_y = response_json['properties']['gridY']

            return (grid_id, grid_x, grid_y)

        return (None, None, None)

    def _get_weather_forecast(self, grid_id, grid_x, grid_y):
        """
        Lookup the weather forecast for the weather points

        Uses the https://www.weather.gov/documentation/services-web-api API
        """
        response = requests_get(f'https://api.weather.gov/gridpoints/{grid_id}/{grid_x},{grid_y}/forecast/hourly')

        if response.status_code == 200:
            response_json = response.json()

            return response_json['properties']['periods'][0]

        return None

    def get_current_weather_at_home(self):
        """
        Get a simple summary of weather data for the Persons address
        """
        lat, lng = self._get_lat_lng()

        if lat and lng:
            grid_id, grid_x, grid_y = self._get_weather_points(lat, lng)
            
            if grid_id and grid_x and grid_y:
                weather_forecast = self._get_weather_forecast(grid_id, grid_x, grid_y)

                return {
                    'temperature': weather_forecast['temperature'],
                    'temperature_unit': weather_forecast['temperatureUnit'],
                    'wind_speed': weather_forecast['windSpeed'],
                    'wind_direction': weather_forecast['windDirection'],
                    'summary': weather_forecast['shortForecast'],
                }

        return None

    def bio(self):
        """
        Return a simple bio of the Person
        """
        weather = self.get_current_weather_at_home()

        bio = f"{self.name} was born on {self.birth_date} and is currently {self.age()} years old. {self.short_name} lives at {self.address}"

        if weather:
            bio += f" where the weather currently is {weather['summary']} at {weather['temperature']}{weather['temperature_unit']}."
        else:
            bio += "."

        return bio
