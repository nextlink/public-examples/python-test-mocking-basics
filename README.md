# Python Test Mocking Basics
An example class with tests showing various ways of utilizing mock data.

## Setup
1. Clone repository
2. Create virtual environment and install `requirements.txt`
3. Run tests with: `python -m unittest discover`

Geocoding data from the US Census Geocoding Services API
https://geocoding.geo.census.gov/

Weather data from the National Weather Service API
https://www.weather.gov/documentation/services-web-api
