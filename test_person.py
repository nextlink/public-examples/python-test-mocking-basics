import datetime
import unittest

from dataclasses import dataclass
from unittest import mock

from person import Person


@dataclass
class MockRequestsResponse:
    status_code: int
    data: object

    def json(self):
        return self.data


class PersonTests(unittest.TestCase):
    def setUp(self):
        self.person = Person(name='John Doe', birth_date=datetime.date(2000, 1, 20), address='2347 Main Street Billings MT 59105')

    def test_person_short_name(self):
        self.assertEqual(self.person.short_name, 'John')

        self.person.name = 'Jane Doe Jr'

        self.assertEqual(self.person.short_name, 'Jane')

    @mock.patch('person.date')
    def test_person_age(self, mock_date):
        mock_date.today = mock.MagicMock(return_value=datetime.date(2021, 8, 15))

        self.assertEqual(self.person.age(), 21.6)

        self.person.birth_date = datetime.date(1985, 10, 18)

        self.assertEqual(self.person.age(), 35.8)

    @mock.patch('person.requests_get', side_effect=[
        MockRequestsResponse(status_code=200, data={'result': {'input': {'benchmark': {'id': '4', 'benchmarkName': 'Public_AR_Current', 'benchmarkDescription': 'Public Address Ranges - Current Benchmark', 'isDefault': False}, 'address': {'address': '2347 Main Street Billings MT 59105'}}, 'addressMatches': [{'matchedAddress': '2347 MAIN ST, BILLINGS, MT, 59105', 'coordinates': {'x': -108.46723, 'y': 45.84123}, 'tigerLine': {'tigerLineId': '181478234', 'side': 'L'}, 'addressComponents': {'fromAddress': '2431', 'toAddress': '2301', 'preQualifier': '', 'preDirection': '', 'preType': '', 'streetName': 'MAIN', 'suffixType': 'ST', 'suffixDirection': '', 'suffixQualifier': '', 'city': 'BILLINGS', 'state': 'MT', 'zip': '59105'}}]}}),
        MockRequestsResponse(status_code=200, data={'result': {'input': {'benchmark': {'id': '4', 'benchmarkName': 'Public_AR_Current', 'benchmarkDescription': 'Public Address Ranges - Current Benchmark', 'isDefault': False}, 'address': {'address': '1234 Main Street Billings MT 59105'}}, 'addressMatches': []}}),
        MockRequestsResponse(status_code=500, data={'error': '500 server error'}),
    ])
    def test_person_get_lat_lng(self, mock_requests_get):
        lat, lng = self.person._get_lat_lng()

        self.assertEqual(lat, 45.84123)
        self.assertEqual(lng, -108.46723)

        mock_requests_get.assert_called_once_with('https://geocoding.geo.census.gov/geocoder/locations/onelineaddress', {
            'benchmark': 'Public_AR_Current',
            'vintage': 'Current',
            'format': 'json',
            'address': '2347 Main Street Billings MT 59105',
        })

        self.person.address = '1234 Main Street Billings MT 59105'

        lat, lng = self.person._get_lat_lng()

        self.assertIsNone(lat)
        self.assertIsNone(lng)

        mock_requests_get.assert_called_with('https://geocoding.geo.census.gov/geocoder/locations/onelineaddress', {
            'benchmark': 'Public_AR_Current',
            'vintage': 'Current',
            'format': 'json',
            'address': '1234 Main Street Billings MT 59105',
        })

        lat, lng = self.person._get_lat_lng()

        self.assertIsNone(lat)
        self.assertIsNone(lng)

    @mock.patch('person.requests_get', side_effect=[
        MockRequestsResponse(status_code=200, data={'properties': {'@id': 'https://api.weather.gov/points/45.8412,-108.4672', '@type': 'wx:Point', 'cwa': 'BYZ', 'forecastOffice': 'https://api.weather.gov/offices/BYZ', 'gridId': 'BYZ', 'gridX': 103, 'gridY': 93, 'forecast': 'https://api.weather.gov/gridpoints/BYZ/103,93/forecast', 'forecastHourly': 'https://api.weather.gov/gridpoints/BYZ/103,93/forecast/hourly', 'forecastGridData': 'https://api.weather.gov/gridpoints/BYZ/103,93', 'observationStations': 'https://api.weather.gov/gridpoints/BYZ/103,93/stations', 'relativeLocation': {'type': 'Feature', 'geometry': {'type': 'Point', 'coordinates': [-108.395879, 45.8316469]}, 'properties': {'city': 'Lockwood', 'state': 'MT', 'distance': {'value': 5626.4684104606, 'unitCode': 'unit:m'}, 'bearing': {'value': 280, 'unitCode': 'unit:degrees_true'}}}, 'forecastZone': 'https://api.weather.gov/zones/forecast/MTZ235', 'county': 'https://api.weather.gov/zones/county/MTC111', 'fireWeatherZone': 'https://api.weather.gov/zones/fire/MTZ128', 'timeZone': 'America/Denver', 'radarStation': 'KBLX'}}),
        MockRequestsResponse(status_code=404, data={'path': '/points/None,None', 'correlationId': 'd56d337e-c81b-4961-b203-c20abb8f5cce', 'title': 'Not Found', 'type': 'https://api.weather.gov/problems/NotFound', 'status': 404, 'detail': "'/points/None,None' is not a valid resource path", 'instance': 'https://api.weather.gov/requests/d56d337e-c81b-4961-b203-c20abb8f5cce'}),
    ])
    def test_person_get_weather_points(self, mock_requests_get):
        grid_id, grid_x, grid_y = self.person._get_weather_points(45.84123, -108.46723)

        self.assertEqual(grid_id, 'BYZ')
        self.assertEqual(grid_x, 103)
        self.assertEqual(grid_y, 93)

        mock_requests_get.assert_called_once_with('https://api.weather.gov/points/45.84123,-108.46723')

        grid_id, grid_x, grid_y = self.person._get_weather_points(None, None)

        self.assertIsNone(grid_id)
        self.assertIsNone(grid_x)
        self.assertIsNone(grid_y)

        mock_requests_get.assert_called_with('https://api.weather.gov/points/None,None')

    @mock.patch('person.requests_get', side_effect=[
        MockRequestsResponse(status_code=200, data={'properties': {'updated': '2021-08-23T20:14:10+00:00', 'units': 'us', 'forecastGenerator': 'HourlyForecastGenerator', 'generatedAt': '2021-08-24T00:10:47+00:00', 'updateTime': '2021-08-23T20:14:10+00:00', 'validTimes': '2021-08-23T14:00:00+00:00/P7DT22H', 'elevation': {'value': 967.1304, 'unitCode': 'unit:m'}, 'periods': [{'number': 1, 'name': '', 'startTime': '2021-08-23T18:00:00-06:00', 'endTime': '2021-08-23T19:00:00-06:00', 'isDaytime': False, 'temperature': 87, 'temperatureUnit': 'F', 'temperatureTrend': None, 'windSpeed': '13 mph', 'windDirection': 'WSW', 'icon': 'https://api.weather.gov/icons/land/night/tsra_hi,20?size=small', 'shortForecast': 'Areas Of Smoke', 'detailedForecast': ''}, {'number': 2, 'name': '', 'startTime': '2021-08-23T19:00:00-06:00', 'endTime': '2021-08-23T20:00:00-06:00', 'isDaytime': False, 'temperature': 84, 'temperatureUnit': 'F', 'temperatureTrend': None, 'windSpeed': '13 mph', 'windDirection': 'N', 'icon': 'https://api.weather.gov/icons/land/night/tsra_hi,20?size=small', 'shortForecast': 'Areas Of Smoke', 'detailedForecast': ''}, {'number': 3, 'name': '', 'startTime': '2021-08-23T20:00:00-06:00', 'endTime': '2021-08-23T21:00:00-06:00', 'isDaytime': False, 'temperature': 78, 'temperatureUnit': 'F', 'temperatureTrend': None, 'windSpeed': '13 mph', 'windDirection': 'N', 'icon': 'https://api.weather.gov/icons/land/night/tsra_hi,20?size=small', 'shortForecast': 'Areas Of Smoke', 'detailedForecast': ''}]}}),
        MockRequestsResponse(status_code=404, data={'path': '/gridpoints/None/None,None/forecast/hourly', 'correlationId': '569c079a-0bed-4e71-87f8-4808c2a4da75', 'title': 'Not Found', 'type': 'https://api.weather.gov/problems/NotFound', 'status': 404, 'detail': "'/gridpoints/None/None,None/forecast/hourly' is not a valid resource path", 'instance': 'https://api.weather.gov/requests/569c079a-0bed-4e71-87f8-4808c2a4da75'}),
    ])
    def test_person_get_weather_forecast(self, mock_requests_get):
        weather_forecast = self.person._get_weather_forecast('BYZ', 103, 93)

        self.assertDictEqual(weather_forecast, {
            'detailedForecast': '',
            'endTime': '2021-08-23T19:00:00-06:00',
            'icon': 'https://api.weather.gov/icons/land/night/tsra_hi,20?size=small',
            'isDaytime': False,
            'name': '',
            'number': 1,
            'shortForecast': 'Areas Of Smoke',
            'startTime': '2021-08-23T18:00:00-06:00',
            'temperature': 87,
            'temperatureTrend': None,
            'temperatureUnit': 'F',
            'windDirection': 'WSW',
            'windSpeed': '13 mph',
        })

        mock_requests_get.assert_called_once_with('https://api.weather.gov/gridpoints/BYZ/103,93/forecast/hourly')

        weather_forecast = self.person._get_weather_forecast(None, None, None)

        self.assertIsNone(weather_forecast)

        mock_requests_get.assert_called_with('https://api.weather.gov/gridpoints/None/None,None/forecast/hourly')

    @mock.patch.object(Person, '_get_weather_forecast', return_value={
        'detailedForecast': '',
        'endTime': '2021-08-23T19:00:00-06:00',
        'icon': 'https://api.weather.gov/icons/land/night/tsra_hi,20?size=small',
        'isDaytime': False,
        'name': '',
        'number': 1,
        'shortForecast': 'Areas Of Smoke',
        'startTime': '2021-08-23T18:00:00-06:00',
        'temperature': 87,
        'temperatureTrend': None,
        'temperatureUnit': 'F',
        'windDirection': 'WSW',
        'windSpeed': '13 mph',
    })
    @mock.patch.object(Person, '_get_weather_points', return_value=('BYZ', 103, 93))
    @mock.patch.object(Person, '_get_lat_lng', return_value=(45.84123, -108.46723))
    def test_person_get_current_weather_at_home(self, mock_get_lat_lng, mock_get_weather_points, mock_get_weather_forecast):
        weather = self.person.get_current_weather_at_home()

        self.assertDictEqual(weather, {
            'temperature': 87,
            'temperature_unit': 'F',
            'wind_speed': '13 mph',
            'wind_direction': 'WSW',
            'summary': 'Areas Of Smoke',
        })

        mock_get_lat_lng.assert_called_once_with()
        mock_get_weather_points.assert_called_once_with(45.84123, -108.46723)
        mock_get_weather_forecast.assert_called_once_with('BYZ', 103, 93)

    @mock.patch.object(Person, '_get_weather_forecast', return_value={
        'detailedForecast': '',
        'endTime': '2021-08-23T19:00:00-06:00',
        'icon': 'https://api.weather.gov/icons/land/night/tsra_hi,20?size=small',
        'isDaytime': False,
        'name': '',
        'number': 1,
        'shortForecast': 'Areas Of Smoke',
        'startTime': '2021-08-23T18:00:00-06:00',
        'temperature': 87,
        'temperatureTrend': None,
        'temperatureUnit': 'F',
        'windDirection': 'WSW',
        'windSpeed': '13 mph',
    })
    @mock.patch.object(Person, '_get_weather_points', return_value=('BYZ', 103, 93))
    @mock.patch.object(Person, '_get_lat_lng', return_value=(45.84123, -108.46723))
    @mock.patch('person.date')
    def test_person_bio(self, mock_date, *args):
        mock_date.today = mock.MagicMock(return_value=datetime.date(2021, 8, 15))

        bio = self.person.bio()

        self.assertEqual(bio, 'John Doe was born on 2000-01-20 and is currently 21.6 years old. John lives at 2347 Main Street Billings MT 59105 where the weather currently is Areas Of Smoke at 87F.')
